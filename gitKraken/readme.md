#Stahneme si a nainstalujeme GitKraken

IDE = integrated development environment


* zaregistrujeme se na jakémkoliv portalu poskytující git
* otevřeme si gitKraken
* [File->Clone repo](https://bitbucket.org/mendelu/zoozs2018_xostrize/src/master/gitKraken/clone.png)
    * vyplníme url kterou získáme z repozitářové stránky (zpravidla končí koncovkou .git) 
    * https://Francimore@bitbucket.org/mendelu/zoozs2018_xostrize.git
    * v počítači si vybereme cestu kam chceme aby se nám to uložilo 
    * stiskneme clone repo


v tento okamžik se nám dohrnula celá struktůra vzdáleného repozitáře. Vesele si vyvíjíme a pravidelně komitujeme (pokud se jedná o zdrojový kod tak vždy funkční celky)

* v okamžiku kdy používáme nějaké IDE, tak je nechtěné aby se nám do vzdáleného repozitáře kopírovali i soubory které nejsou potřeba pro sestavení a přeložení projektu (viz třeba nastavení IDE atp.).
    * k tomu nám slouží definice souboru .gitIgnore
    * pro účely Kurzu zoo nam postačí si přidat do svého .gitIgnore následující
>.*
>*.cmake
>*.bin
>*.rsp
>*.a
>*.make
>*.check_cache
>*.obj
>*.internal
>*.exe
>*CMakeCXXCompilerId.cpp
>*CMakeCache.txt
>*CMakeOutput.log
>*/.idea/
>*.bcp
>*Makefile
>*TargetDirectories.txt
>*.cxx
>*.c
>*clion-log.txt
>*clion-environment.txt
>*link.txt
>*.out
>*Makefile2
>*progress.marks
>*.cbp
    * necháme si zobrazit skryté soubory a vepíšeme řádky víše do .gitignore uložíme a pushneme do repozitáře..... 
    * v okamžiku kdy máme existující projekt nebo si založíme nový tak by měl všechny zbytečné soubory git repozitář ignorovat
    * refrešneme okno gitkraken ctrl+r a vidíme že všechny [nepotřebné soubory se nám odfiltrovali](https://bitbucket.org/mendelu/zoozs2018_xostrize/src/master/gitKraken/filter.png)


* dobrou praxí při verzování bývá že při příchodu k počítači si dáte pull a při odchodu od počítače se dává push (všechno mezi tím se řeší commity, které by měli být průběžné a odladěné)



