#include <iostream>
using namespace std;

class Auto{
//private:
    int m_aktualniCena;
    int m_amortizaceNaKm;
    int m_ujetoKm;

public:
    Auto(int vychoziCena) {
        setVychoziCena(vychoziCena);
        setVychoziAmortizaceNaKm(20);
        m_ujetoKm = 0;
    }

    Auto(int vychoziCena, int amortizaceNaKm){
        setVychoziCena(vychoziCena);
        setVychoziAmortizaceNaKm(amortizaceNaKm);
        m_ujetoKm = 0;
    }

    void evidujJizdu(int ujetoKm){
        if (ujetoKm > 0) {
            // spocitat amortizaci za tu jizdu
            int amortizace = spocitejAmortizaci(ujetoKm);
            // odesit amortizaci z ceny
            //m_aktualniCena = m_aktualniCena - amortizace;
            m_aktualniCena -= amortizace;
            // odcist km ze stavu km auta
            m_ujetoKm += ujetoKm;
        } else {
            cout << "Pozor, zaporne km" << endl;
        }
    }

    void printInfo(){
        cout << "Cena: " << m_aktualniCena << endl;
        cout << "Km: " << m_ujetoKm << endl;
    }

    bool jeAmortizovano(){
        if (m_aktualniCena > 0){
            return false;
        } else {
            return true;
        }
        //return m_aktualniCena <= 0;
    }

private:
    int spocitejAmortizaci(int ujetoKm){
        return ujetoKm*m_amortizaceNaKm;
    }

    void setVychoziCena(int vychoziCena){
        if (vychoziCena > 0) {
            m_aktualniCena = vychoziCena;
        } else {
            cout << "Vychozi cena musi byt > 0, nastaveno na 100000" << endl;
            m_aktualniCena = 100000;
        }
    }

    void setVychoziAmortizaceNaKm(int vychoziAmortizace){
        if (vychoziAmortizace > 0) {
            m_amortizaceNaKm = vychoziAmortizace;
        } else {
            cout << "Vychozi amortizace musi byt > 0, nastaveno na 20 Kc" << endl;
            m_amortizaceNaKm = 20;
        }
    }
};



int main() {
    Auto* ford = new Auto(10000, 1000);
    ford->evidujJizdu(5);
    ford->printInfo();
    ford->evidujJizdu(6);
    ford->printInfo();

    if (ford->jeAmortizovano()){
        cout << "Je amortizovano" << endl;
    }

    delete ford;
    return 0;
}