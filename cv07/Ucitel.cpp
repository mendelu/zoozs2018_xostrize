//
// Created by xproch17 on 09.11.2018.
//

#include "Ucitel.h"

Ucitel::Ucitel(std::string jmeno, std::string rodneCislo,
        std::string ustav):Osoba(jmeno, rodneCislo){
    setUstav(ustav);
}

void Ucitel::setUstav(std::string ustav){
    if (ustav != ""){
        m_ustav = ustav;
    } else {
        std::cout << "Nelze ulozit prazdny ustav" << std::endl;
    }
}

std::string Ucitel::getUstav(){
    return m_ustav;
}

void Ucitel::printInfo(){
    //std::cout << "Ucitel\nJmeno: " << m_jmeno << std::endl;
    std::cout << "Ucitel\nJmeno: " << getJmeno() << std::endl;
}