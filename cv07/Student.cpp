//
// Created by xproch17 on 09.11.2018.
//

#include "Student.h"

Student::Student(std::string jmeno,
        std::string rodneCislo,
        int semestr, float prumer):
        Osoba(jmeno, rodneCislo){
    setPrumer(prumer);
    m_semestr = semestr; // TODO kontrola pomoci private setru?
}

void Student::zvysSemestr(){
    m_semestr += 1;
}

void Student::printInfo(){
    std::cout << "Student\nJmeno: " << getJmeno() << std::endl;

}

float Student::getPrumer(){
    return m_prumer;
}

int Student::getSemestr(){
    return m_semestr;
}

void Student::setPrumer(float prumer){
    if ((prumer >= 1.0) and (prumer <= 5.0)){
        m_prumer = prumer;
    } else {
        std::cout << "Prumer musi byt [1,5]" << std::endl;
    }
}