//
// Created by xproch17 on 09.11.2018.
//

#ifndef CV07_UIS_H
#define CV07_UIS_H

#include <vector>
#include "Ucitel.h"

class Uis {
    std::vector<Ucitel*> m_ucitele;
public:
    Uis();
    ~Uis();
    void addUcitel(std::string jmeno, std::string rodneCislo, std::string ustav);
    void printUcitele();
};


#endif //CV07_UIS_H
