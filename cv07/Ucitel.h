//
// Created by xproch17 on 09.11.2018.
//

#ifndef CV07_UCITEL_H
#define CV07_UCITEL_H

#include "Osoba.h"

class Ucitel : public Osoba {
    std::string m_ustav;
public:
    Ucitel(std::string jmeno, std::string rodneCislo, std::string ustav);
    void setUstav(std::string ustav);
    std::string getUstav();
    void printInfo();
};


#endif //CV07_UCITEL_H
