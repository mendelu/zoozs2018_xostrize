//
// Created by xproch17 on 16.11.2018.
//

#include "Technik.h"

int Technik::getDniDovolene(int cerpanoDni){
    //int maxDniDovolene = 30; // level 1
    const int maxDniDovolene = 30; // level 2

    return maxDniDovolene - cerpanoDni;
}

int Technik::getPlat(int letVeFirme, int studenVzdelani){
    const int zakladniPlat = 30000;
    const int rocniBonus = 1000;

    return zakladniPlat + letVeFirme * rocniBonus;
}