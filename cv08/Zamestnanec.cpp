//
// Created by xproch17 on 16.11.2018.
//

#include "Zamestnanec.h"


Zamestnanec::Zamestnanec(int letVeFirme, int stupenVzdelani,
                         TypPozice pracovniPozice){
    m_letVeFirme = letVeFirme; // TODO kontrola vstupu
    m_stupenVzdelani = stupenVzdelani;
    m_cerpanoDniDovolene = 0;

    m_pozice = nullptr; // abych rekl metode, ze jeste nic nema mazat
    zmenPracovniPozici(pracovniPozice);
}

Zamestnanec::~Zamestnanec(){
    delete m_pozice;
}

int Zamestnanec::getPlat(){
    // tady by vam rostla serie ifu
    return m_pozice->getPlat(m_letVeFirme, m_stupenVzdelani);
}

int Zamestnanec::getZbyvaDniDovolene(){
    // tady by vam rostla serie ifu
    return m_pozice->getDniDovolene(m_cerpanoDniDovolene);
}

void Zamestnanec::evidujDovolenou(int cerpaniDni){
    m_cerpanoDniDovolene += cerpaniDni;
}

void Zamestnanec::zmenPracovniPozici(TypPozice pracovniPozice){
    if (m_pozice != nullptr) {
        delete m_pozice;
    }

    if (pracovniPozice == TypPozice::Technik){
        m_pozice = new Technik();
    } else {
        m_pozice = new Akademik();
    }
}
