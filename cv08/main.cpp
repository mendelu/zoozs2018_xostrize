#include <iostream>

#include "Zamestnanec.h"

int main() {
    // Kompozice a zapouzdreni krasne skryva vnitrni fungovani
    Zamestnanec* david = new Zamestnanec(5, 4, TypPozice::Akademik);
    std::cout << "Plat: " << david->getPlat() <<std::endl;
    std::cout << "Dovolena: " << david->getZbyvaDniDovolene() <<std::endl;

    david->zmenPracovniPozici(TypPozice::Technik);
    std::cout << "Plat: " << david->getPlat() <<std::endl;
    std::cout << "Dovolena: " << david->getZbyvaDniDovolene() <<std::endl;

    delete david;
    return 0;
}