//
// Created by xproch17 on 16.11.2018.
//

#ifndef CV08_ZAMESTNANEC_H
#define CV08_ZAMESTNANEC_H

#include "PracovniPozice.h"
#include "Technik.h"
#include "Akademik.h"

enum class TypPozice{
    Technik, Akademik
};

class Zamestnanec {
    int m_cerpanoDniDovolene;
    int m_letVeFirme;
    int m_stupenVzdelani;

    PracovniPozice* m_pozice;
public:
    Zamestnanec(int letVeFirme, int stupenVzdelani, TypPozice pracovniPozice);
    ~Zamestnanec();

    int getPlat();
    int getZbyvaDniDovolene();
    void evidujDovolenou(int cerpaniDni);

    void zmenPracovniPozici(TypPozice pracovniPozice);
};


#endif //CV08_ZAMESTNANEC_H
