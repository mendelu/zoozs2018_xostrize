//direktiva připojující systémovou knihovnu input output stream
#include <iostream>

//direktiva připojující jmenný prostor std pro to abyjsme nemuseli neustale psát std::cout std::string std::cin std::endl apod.
using namespace std;

/***
 *
 * Obecký předpis třídy na které si ukážeme pár základních pravidel
 * Do části public: přijde všechno co má být vidět vně třídy (standartní viditelnost je pouze uvnitř třídy
 *
 * @author xostrize
 */
class Trida{
public:

    /***
     * Klíčové slovo void říká překladači že tato metoda ve skutečnosti nic nevrací
     * a proto si můžeme v metodě dělat co se nám zlíbí. Typický příklad jsou atomické operace např. výpis, nastavení atp.
     * metoda nic nepřebírá a nic nevrací.
     * @author xostrize
     */

    void metoda(){
        cout<< "metoda která nic nevrací" << endl;
    }

    /***
     *
     * Tato metoda definuje jako návratový typ integer což je typicky číslo
     *
     * @return číslo zadané uživatelem.
     */
    int metoda1(){

        cout << "zadej cislo, aktualne je inicializovana: " << i << endl;

        cin >> i;

        return i;
    }
};

int main() {

    Auto* skoda = new Auto();



    //1] musim inicializovat promene
    //2] musim secist promene
    //...
    //n] konec programu

    cout << "hodnota j je: " << skoda->printInfo(0) << endl;

    delete skoda;

    return 0;
}