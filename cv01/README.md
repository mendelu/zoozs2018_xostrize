#Úvod do prostředí clion

IDE = integrated development environment

1. nainstalujeme si clion (clion je pouze IDE)
##Windows
1. nainstalujeme pro OS windows [chockolatey](https://chocolatey.org/)
1. otevřeme si cmd s právy administrátora a zadáme příkaz:
>choco install cmake
##Linux
yum/aptitude install gcc g++ cmake

##clion
1. otevřeme si clion 
    * pro trial učely na 30 minut zdarma
    * [jinak nutné registrovat, pro studenty je zdarma](https://www.jetbrains.com/student/)
1. file->settings->Build,Execution,Deployment->Toolchain
    * buď se vám to nalezne
    * nebo si vyspecifikujete cesty ke všem potřebným kompilerům

1. file->new project -> specify path to your folder -> create
    * v levé části vidíme průzkumníka souborů
    * v pravé části vidíme editor
    * nahoře vidíme kladívko a šipečku kterou spustíme program
