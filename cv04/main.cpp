#include <iostream>
using namespace std;

class ErrorLog{
    static ErrorLog* s_log;
    string m_errors;
public:
    static ErrorLog* getErrorLog(){
        if (s_log == nullptr){
            s_log = new ErrorLog();
        }
        return s_log;
    }

    void logError(string where, string what){
        m_errors += "- " + where + ": " + what + "\n";
    }

    string getErrors(){
        return m_errors;
    }
private:
    ErrorLog(){
        m_errors = "Error log: \n";
    }
};

ErrorLog* ErrorLog::s_log = nullptr;


class Drak{
    int m_zivoty;
    int m_sila;
    int m_obrana;
public:
    Drak(int sila, int obrana){
        setSila(sila);
        setObrana(obrana);
        m_zivoty = 100;
    }

    int getZivoty(){
        return m_zivoty;
    }

    int getSila(){
        return m_sila;
    }

    int getObrana(){
        return m_obrana;
    }

    void uberZivoty(int kolik){
        // TODO kontrola zaporne hodnoty
        // TODO nepadam pod nulu?
        m_zivoty -= kolik;
    }

private:
    void setSila(int sila){
        if ((sila >= 0) and (sila <= 100)) {
            m_sila = sila;
        } else {
            ErrorLog* log = ErrorLog::getErrorLog();
            log->logError("Drak::setSila", "Sila mimo rozsah <0,100>. Nastavena na 0.");
            m_sila = 0;
        }
    }

    void setObrana(int obrana){
        if ((obrana >= 0) and (obrana <= 200)) {
            m_obrana = obrana;
        } else {
            ErrorLog* log = ErrorLog::getErrorLog();
            log->logError("Drak::setObrana", "Obrana mimo rozsah <0,200>. Nastavena na 0.");
            m_obrana = 0;
        }
    }
};

class Rytir{
    string m_jmeno;
    int m_zivoty;
    int m_sila;
    int m_obrana;
public:
    Rytir(string jmeno, int sila, int obrana){
        // TODO dodelej kontroly vstupu
        m_jmeno = jmeno;
        m_sila = sila;
        m_obrana = obrana;
        m_zivoty = 100;
    }

    string getJmeno(){
        return m_jmeno;
    }

    int getZivoty(){
        return m_zivoty;
    }

    int getSila(){
        return m_sila;
    }

    int getObrana(){
        return m_obrana;
    }

    // pozor drak musi byt nad rytirem
    // nedavat do draka naopka ukazatel na rytire
    void boj(Drak* protivnik){
        if (protivnik->getSila() > m_obrana){
            m_zivoty -= (protivnik->getSila()-m_obrana);
        }

        if (protivnik->getObrana() < m_sila){
            // muzete pouzit pomocnou promennou, pokud se necitite
            int rozdil = m_sila-protivnik->getObrana();
            protivnik->uberZivoty(rozdil);
        }
    }
};

int main() {
    Drak* smak = new Drak(150, 60);
    Rytir* artus = new Rytir("Artus", 70, 50);

    artus->boj(smak);
    std::cout << artus->getJmeno() << ": "
              << artus->getZivoty() << endl;

    std::cout << "Smak: "
              << smak->getZivoty() << endl;

    ErrorLog* log = ErrorLog::getErrorLog();
    cout << log->getErrors();

    delete smak;
    delete artus;
    return 0;
}