#include <iostream>
#include "MagBuilder.h"
#include "HrdinaDirector.h"

int main() {
    MagBuilder* magove = new MagBuilder();
    HrdinaDirector* tvurce = new HrdinaDirector(magove);
    //HrdinaDirector* tvurce = new HrdinaDirector(new MagBuilder);

    Hrdina* nejakyMag = tvurce->createHrdina("Sarsim");

    //tvurce->setBuilder(new ZlodejBuilder);
    //...

    delete magove;
    delete tvurce;
    return 0;
}