//
// Created by xproch17 on 30.11.2018.
//

#include "Hrdina.h"

Hrdina::Hrdina(std::string jmeno, std::string povolani,
        int sila, int obrana){
    // TODO kontrola
    m_jmeno = jmeno;
    m_povolani = povolani;
    m_sila = sila;
    m_obrana = obrana;
}

int Hrdina::getUtok(){
    int silaZbrani = 0;
    for(Zbran* zbran : m_zbrane){
        silaZbrani += zbran->getBonusSily();
    }

    return m_sila + silaZbrani;
}

int Hrdina::getObrana(){
    return m_obrana;
}

void Hrdina::seberZbran(Zbran* zbran){
    m_zbrane.push_back(zbran);
}

void Hrdina::seberLektvar(Lektvar* lektvar){
    m_lektvary.push_back(lektvar);
}

void Hrdina::vypijPosledniLektvar(){
    // TODO homework
}

void Hrdina::printInfo(){
    // TODO homework
}