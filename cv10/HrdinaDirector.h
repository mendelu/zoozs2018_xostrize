//
// Created by xproch17 on 30.11.2018.
//

#ifndef CV10_09H_HRDINADIRECTOR_H
#define CV10_09H_HRDINADIRECTOR_H

#include <iostream>
#include "HrdinaBuilder.h"

class HrdinaDirector {
    HrdinaBuilder* m_builder;
public:
    HrdinaDirector(HrdinaBuilder* builder);
    void setBuilder(HrdinaBuilder* builder);
    Hrdina* createHrdina(std::string jmeno);
};


#endif //CV10_09H_HRDINADIRECTOR_H
