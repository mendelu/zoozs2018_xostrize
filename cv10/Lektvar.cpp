//
// Created by xproch17 on 30.11.2018.
//

#include "Lektvar.h"

Lektvar::Lektvar(int bonusSily, int bonusObrany){
    // TODO kontrola vstupu
    m_bonusSily = bonusSily;
    m_bonusObrany = bonusObrany;
}

int Lektvar::getBonusSily(){
    return m_bonusSily;
}

int Lektvar::getBonusObrany(){
    return m_bonusObrany;
}
