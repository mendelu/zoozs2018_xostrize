//
// Created by xproch17 on 30.11.2018.
//

#ifndef CV10_09H_HRDINA_H
#define CV10_09H_HRDINA_H

#include <iostream>
#include <vector>
#include "Lektvar.h"
#include "Zbran.h"

class Hrdina {
    std::string m_jmeno;
    std::string m_povolani;
    int m_sila;
    int m_obrana;
    std::vector<Zbran*> m_zbrane;
    std::vector<Lektvar*> m_lektvary;
public:
    Hrdina(std::string jmeno, std::string povolani,
            int sila, int obrana);
    int getUtok();
    int getObrana();
    void seberZbran(Zbran* zbran);
    void seberLektvar(Lektvar* lektvar);
    void vypijPosledniLektvar();
    void printInfo();
};


#endif //CV10_09H_HRDINA_H
