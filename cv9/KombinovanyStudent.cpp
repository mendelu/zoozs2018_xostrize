//
// Created by Mikulas Muron on 26/11/2018.
//

#include "KombinovanyStudent.h"

KombinovanyStudent::KombinovanyStudent(string jmeno, OborStudia obor, int studujeLet, int prumer, int dojezdovaVzdalenost) : Student(jmeno, obor, studujeLet, prumer) {
    m_dojezdovaVzdalenost = dojezdovaVzdalenost;
}

int KombinovanyStudent::getDojezdovaVzdalenost() {
    return m_dojezdovaVzdalenost;
}

int KombinovanyStudent::getStipendium() {
    return  (1.0 / getPrumer() * 5000) + (getDojezdovaVzdalenost() * 100);
}

bool KombinovanyStudent::prodluzujeStudium() {
    if(getPocetLetStudia() > KombinovanyStudent::s_pocetLetStudia){
        return true;
    }
    return false;
}

void KombinovanyStudent::printInfo() {
    std::cout << "Kombinovany student " << getJmeno() << " se stipendiem " << getStipendium() << std::endl;
}