//
// Created by Mikulas Muron on 26/11/2018.
//

#include "SeznamStudentu.h"


std::vector<Student*> SeznamStudentu::s_studenti;

void SeznamStudentu::pridej(Student* novyStudent) {
    s_studenti.push_back(novyStudent);
}

void SeznamStudentu::pridej(string jmeno, int dobaStudia, OborStudia obor, int dojezdovaVzdalenost) {
    Student* novyStudent = Student::getStudent(jmeno,dobaStudia,obor,dojezdovaVzdalenost);
    s_studenti.push_back(novyStudent);
}

std::vector<Student*> SeznamStudentu::najdiPodleJmena(std::string jmeno) {
    std::vector<Student*> nalezeniStudenti;
    for(Student* s : s_studenti){
        if(s->getJmeno() == jmeno){
            nalezeniStudenti.push_back(s);
        }
    }
    return nalezeniStudenti;
}

std::vector<Student*> SeznamStudentu::najdiPodleOboru(OborStudia obor) {
    std::vector<Student*> nalezeniStudenti;
    for(Student* s : s_studenti){
        if(s->getObor() == obor){
            nalezeniStudenti.push_back(s);
        }
    }
    return nalezeniStudenti;
}

int SeznamStudentu::celkovaSumaStipendii() {
    int sum = 0;
    for(Student* s: s_studenti){
        sum += s->getStipendium();
    }
    return sum;
}

void SeznamStudentu::printInfo() {
    for(Student* s : s_studenti){
        s->printInfo();
    }
}

int SeznamStudentu::kolikProdluzujeStudium() {
    int pocet = 0;
    for(Student* s: s_studenti){
        if(s->prodluzujeStudium()){
            pocet += 1;
        }
    }
    return pocet;
}

std::vector<Student*> SeznamStudentu::getSeznam() {
    return s_studenti;
}